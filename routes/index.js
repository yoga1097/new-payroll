var express = require('express');

var router = express.Router();


var Sequelize=require("sequelize");
var sequelize = new Sequelize('temp', 'root', 'root', {
  host: 'localhost',
  dialect: 'mariadb',
  pool: {
    max: 10,
    min: 0,
    idle: 10000
  },
});


//product schema
var product = sequelize.define('product', {
product_name : {type : Sequelize.STRING,required : true}
 },
  {freezeTableName: true /* Model tableName will be the same as the model name*/
});


//SALARY REGULAR TABLE SCHEMA
var salaryregulartable = sequelize.define('salaryregulartable', {

  regular_shift_amount: {type: Sequelize.INTEGER, allowNull:false},

morning_shift_amount: {type: Sequelize.INTEGER, allowNull:false},

evening_shift_amount: {type: Sequelize.INTEGER, allowNull:false},
},
  {freezeTableName: true /* Model tableName will be the same as the model name*/
});



//SALARY CONTRACTOR TABLE SCHEMA
var salarycontractortable = sequelize.define('salarycontractortable', {
  
  cost_unit: {type: Sequelize.INTEGER, allowNull:false},

  unit: { type : Sequelize.STRING,required : true }
},
  {freezeTableName: true /* Model tableName will be the same as the model name*/
});
salarycontractortable.belongsTo(product,{foreignKey: 'productId' });



//designation schema
var designation = sequelize.define('designation', {
  type_of_employee: {type: Sequelize.STRING, allowNull:false},

  designation_name: {type:Sequelize.STRING(75),allowNull:false,unique:true}

  //salary_id:{type:Sequelize.INTEGER,  unique:true,autoIncrement: true}
},{freezeTableName: true
});
designation.belongsTo(salarycontractortable,{foreignKey: 'salarycontractortableId'});
designation.belongsTo(salaryregulartable,{foreignKey: 'salaryregulartableId'});



//employee schema
var employee = sequelize.define('employee', {

    id: {type:Sequelize.INTEGER  ,  allowNull: false, primaryKey:true,autoIncrement:true },

    name: {type: Sequelize.STRING, allowNull:false},

    
    phone_no :{type: Sequelize.BIGINT   , allowNull:false},

    first_credit:{type:Sequelize.INTEGER  ,allowNull:false},


  },
  {freezeTableName: true /* Model tableName will be the same as the model name*/
});
employee.belongsTo(designation ,{foreignKey: 'designationId'}); 


//ATTENDANCE SCHEMA


var attendance =sequelize.define('attendance', {
  date :{type: Sequelize.DATEONLY , allowNull:false},
  food_cost:{type:Sequelize.INTEGER,allowNull:false},
  credit_id:{type:Sequelize.INTEGER ,allowNull:false},
  regular_shift:{type:Sequelize.BOOLEAN},
  morning_shift:{type:Sequelize.BOOLEAN},
  evening_shift:{type:Sequelize.BOOLEAN},
  no_of_units:{type:Sequelize.INTEGER}

 },
  {freezeTableName: true /* Model tableName will be the same as the model name*/
});
attendance.belongsTo(employee,{foreignKey: 'employeeId'});


//CREDIT SCHEMA
var credit = sequelize.define('credit', {
  date :{type: Sequelize.DATE ,  defaultValue: Sequelize.NOW},
  given_taken:{type:Sequelize.STRING,allowNull:false},
  credit_amount:{type:Sequelize.INTEGER,allowNull:false},
  other:{type:Sequelize.STRING}
},
  {freezeTableName: true /* Model tableName will be the same as the model name*/
});
credit.belongsTo(employee);




sequelize.query('SET FOREIGN_KEY_CHECKS = 0')
.then(function(){
console.log("FOREIGN_KEY_CHECKS set to 0");
  create_schemas();
});

var create_schemas=function(){
product.sync({force: false}).then(function () {
  // Table created

  salarycontractortable.sync({force: false}).then(function () {
    // Table created

      salaryregulartable.sync({force: false}).then(function () {
      // Table created
        designation.sync({force: false}).then(function () {
        // Table created
        
          employee.sync({force: false}).then(function () {
          // Table created

            attendance.sync({force: false}).then(function () {
            // Table created
              
              credit.sync({force: false}).then(function () {
              // Table created
              });

            });

          });


        });

      });

  });

});
}


/* GET admin home page. */
router.get('/', function(req, res, next) {

  res.render('admin_home');

});



/* GET attendance page. */
router.get('/attendance', function(req, res, next) {


  res.render('attendance');

});


/* GET product page. */

router.get('/product', function(req, res, next) {


  res.render('product');

});


/* POST product page. */

router.post('/product_insert', function(req, res, next) {

sequelize.sync({force:false } ).then(function( ) {
var productinstance= product.build({
product_name:req.body.product_name
    
})
productinstance.save();


res.redirect('/');
//});
});
});

/* GET salary_regular_table page. */
router.get('/salary_regular_table', function(req, res, next) {
  res.render('salary_regular_table');
});



/* GET salary_contract_table page. */
router.get('/salary_contract_table', function(req, res, next) {
product.findAll({}).then(function(doc){

  res.render('salary_contract_table',{items:doc});

});
});



/* GET designation page. */

router.get('/designation', function(req, res, next) {
  product.findAll({}).then(function(doc){
  
  res.render('designation',{items:doc});
});
  

});
/* POST designation entry page. */


router.post('/designation_entry', function(req, res, next) {

var type = req.body.type_of_employee;
if(type == 'Regular')
{


var salary_reg_instance= salaryregulartable.build({
regular_shift_amount:req.body.regular_shift_amount,
morning_shift_amount:req.body. morning_shift_amount,
evening_shift_amount:req.body. evening_shift_amount,
})

salary_reg_instance.save().then(function(data) {
  console.log(data. dataValues.id);
  var designation_instance= designation.build({
  type_of_employee:req.body.type_of_employee,
  designation_name:req.body.designation_name,
  salaryregulartableId:data.dataValues.id
  })

designation_instance.save();
res.redirect('/');



})

  }
else
{
var salary_contract_instance= salarycontractortable.build({
cost_unit:req.body.cost_unit,
unit:req.body. unit,
productId:req.body.product_id
    
})
salary_contract_instance.save().then(function(data) {

var designation_instance= designation.build({
type_of_employee:req.body.type_of_employee,
designation_name:req.body.designation_name,

salarycontractortableId:data.dataValues.id
})
designation_instance.save();
res.redirect('/');

});



  }  
});
/* GET insert_employee_details page. */


  
  


router.get('/insert_employee_details', function(req, res, next) {

  
designation.findAll({}).then(function(doc){
  
  res.render('insert_employee_details',{items:doc});

});


});
/* POST insert page. */

router.post('/insert', function(req, res, next) {

var insert_instance= employee.build({
name:req.body.name,
phone_no:req.body.phone_no,
first_credit:req.body.first_credit,
designationId:req.body.designation_id
    
})  

insert_instance.save().then(function(data){
 console.log(data. dataValues.id);
 
var credit_instance = credit.build({
credit_amount:req.body.first_credit,
other:'first_credit',
given_taken:'taken',
employeeId:data.dataValues.id

})
credit_instance.save();
res.redirect('/');

})




});


/* GET attendance page. */

router.get('/attendance', function(req, res, next) {

  
employee.findAll({}).then(function(doc){
  
  res.render('attendance',{items:doc});

});


});




/* POST attendance entry page. */


router.post('/attendance_entry', function(req, res, next) {

var type = req.body.type_of_employee;
if(type == 'Regular')
{


  var credit1_instance= credit.build({
   
    given_taken:req.body.given_taken,
    credit_amount:req.body.credit_amount,
    other:'NULL',
    employeeId:req.body.employee_id
    

})

credit1_instance.save().then(function(data) {
var attendance_instance= attendance.build({
  employeeId:req.body.employee_id,
  date:req.body.date,
regular_shift:req.body.regular_shift? true : false,
morning_shift:req.body. morning_shift ? true : false,
evening_shift:req.body. evening_shift ? true : false,
food_cost:req.body.food_cost,
 credit_id:data.dataValues.id
 


  })

attendance_instance.save();
res.redirect('/');



})

  }
else
{
 var credit1_instance= credit.build({
   
    given_taken:req.body.given_taken,
    credit_amount:req.body.credit_amount,
    other:'NULL',
    employeeId:req.body.employee_id
    


})

credit1_instance.save().then(function(data) {
var attendance_instance= attendance.build({
  employeeId:req.body.employee_id,
  date:req.body.date,
no_of_units:req.body.no_of_units,
food_cost:req.body.food_cost,
 credit_id:data.dataValues.id
 


  })

attendance_instance.save();
res.redirect('/');



})

  }  
});








/* GET employee details*/
router.get('/get', function(req, res, next) {

  
employee.findAll({}).then(function(doc){
  
  res.render('get',{items:doc});

});


});
module.exports = router;

